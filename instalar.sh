#!/bin/bash

# Verificar si existe alguno de los directorios 'data' o 'data/html'
if [ -d "data" ] || [ -d "data/html" ]; then
  # Comprobar si alguno de los directorios no está vacío
  if [ "$(ls -A data)" ] || [ "$(ls -A data/html)" ]; then
    echo "Advertencia: El directorio 'data' o 'data/html' no está vacío. Continuar puede eliminar el contenido existente."
    read -p "¿Continuar? (s/N): " continuar_opcion
    if [ "$continuar_opcion" != "s" ]; then
      echo "Cancelando operación."
      exit 1
    fi
  fi
else
  # Crear ambos directorios si no existen
  mkdir -p data/html
fi

# Ejecutar 'docker-compose up -d'
docker compose up -d
